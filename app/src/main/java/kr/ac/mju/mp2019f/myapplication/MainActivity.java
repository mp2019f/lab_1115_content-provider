package kr.ac.mju.mp2019f.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Cursor c = getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI, null,
                        null, null, null );
        while( c.moveToNext() ) {
            Log.d("ContentProvider",
                    c.getString(
                            c.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME))
            );
        }

        Cursor d = getContentResolver().query(
                Telephony.Sms.CONTENT_URI, null,
                null, null, null );
        while( d.moveToNext() ) {
            Log.d("ContentProvider",
                    d.getString(
                            d.getColumnIndex(Telephony.Sms.BODY))
            );
        }

    }
}
